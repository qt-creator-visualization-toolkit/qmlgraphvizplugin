import QtQuick 1.1
import org.liveblue.qmlcomponents.graphviz 1.1

Flickable {
    function countChanged() {
        for (var i = 0; i < view.children.length; ++i)
            view.children[i].destroy();
        var boundingRect = model.boundingRect;
        flickable.contentWidth =boundingRect.width;
        flickable.contentHeight =boundingRect.height
        view.width = boundingRect.width;
        view.height = boundingRect.height;
        for (var j = 0; j < model.count; ++j)
        {
            var shape = model.data(j, GraphvizModel.ShapeRole);
            var component;
            if (shape === "" || shape === "ellipse")
                component = Qt.createComponent("GraphvizRectNode.qml");
            var node = component.createObject(view, {"x": boundingRect.width/2, "y": boundingRect.height/2});

            var label = model.data(j, GraphvizModel.DisplayRole);
            node.text = label;

            var size = model.data(j, GraphvizModel.SizeRole);
            node.width = size.width;
            node.height = size.height;

            var position = model.data(j, GraphvizModel.PositionRole);
            node.x = position.x - node.width/2;
            node.y = position.y - node.height/2;
        }
    }
    id: flickable
    property GraphvizModel model;
    Connections {
        target: model
        onCountChanged: countChanged()
    }
    Rectangle {
        id: view
        border.color: "red"
    }
}
