# Try to find location of Qt private headers (see README)

isEmpty(QT_PRIVATE_HEADERS) {
    QT_PRIVATE_HEADERS=$$(QT_PRIVATE_HEADERS)
    isEmpty(QT_PRIVATE_HEADERS) {
        QT_PRIVATE_HEADERS = $$[QT_INSTALL_HEADERS]
    }
}
!isEmpty(QT_PRIVATE_HEADERS) {
    INCLUDEPATH += \
        $${QT_PRIVATE_HEADERS} \
        $${QT_PRIVATE_HEADERS}/QtCore \
        $${QT_PRIVATE_HEADERS}/QtCore/private \
        $${QT_PRIVATE_HEADERS}/QtGui \
        $${QT_PRIVATE_HEADERS}/QtGui/private \
        $${QT_PRIVATE_HEADERS}/QtScript \
        $${QT_PRIVATE_HEADERS}/QtScript/private \
        $${QT_PRIVATE_HEADERS}/QtDeclarative \
        $${QT_PRIVATE_HEADERS}/QtDeclarative/private
}
!exists($${QT_PRIVATE_HEADERS}/QtCore/private/qobject_p.h) {
    warning()
    warning("Graphviz QML plugin cannot be built.")
    warning("The Graphviz QML plugin depends on private headers from QtCore module.")
    warning("To enable it, pass 'QT_PRIVATE_HEADERS=$QTDIR/include' to qmake, where $QTDIR is the build directory of qt.")
    warning("Alternatively, you can also setup the QT_PRIVATE_HEADERS environment variable.")
    error()
}
