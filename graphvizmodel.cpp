#include "graphvizmodel.h"

GraphvizModel::GraphvizModel(QObject *parent)
: QObject(parent),
  m_graphvizType(UnspecifiedType),
  m_graphvizLayout(UnspecifiedLayout),
  m_status(UnspecifiedStatus),
  m_gvc(gvContext()),
  m_graph(0),
  m_isComponentComplete(true),
  m_reply(0),
  m_redirectCount(0)
{
}

GraphvizModel::~GraphvizModel()
{
    agclose(m_graph);
    gvFreeContext(m_gvc);
}

GraphvizModel::GraphvizType GraphvizModel::type() const
{
    return m_graphvizType;
}

GraphvizModel::GraphvizLayout GraphvizModel::layout() const
{
    return m_graphvizLayout;
}

void GraphvizModel::setLayout(const GraphvizLayout &graphLayout)
{
    if (m_graphvizLayout != graphLayout)
    {
        m_graphvizLayout = graphLayout;
        layoutGraph();
        emit graphLayoutChanged(m_graphvizLayout);
    }
}

GraphvizModel::Status GraphvizModel::status() const
{
    return m_status;
}

QUrl GraphvizModel::source() const
{
    return m_source;
}

void GraphvizModel::setSource(const QUrl &source)
{
    if (m_source != source)
    {
        m_source = source;
        if (m_dot.isEmpty())    // m_source is only used if m_dot is not set
            reloadGraph();
        emit sourceChanged();
    }
}

QString GraphvizModel::dot() const
{
    return m_dot;
}

void GraphvizModel::setDot(const QString &dot)
{
    if (m_dot != dot)
    {
        m_dot = dot;
        reloadGraph();
        emit dotChanged();
    }
}

int GraphvizModel::count() const
{
    if (m_graph)
        return agnnodes(m_graph);
    else
        return 0;
}

QString GraphvizModel::errorString() const
{
    return m_errorString;
}

QRect GraphvizModel::boundingRect() const
{
    QString str(agget(m_graph, (char *) "bb"));
    QRect rect;
    if (!str.isEmpty())
    {
        QStringList dim = str.split(',');
        rect.setCoords(dim.at(0).toInt(), dim.at(1).toInt(), dim.at(2).toInt(), dim.at(3).toInt());
    }
    return rect;
}

void GraphvizModel::classBegin()
{
    m_isComponentComplete = false;
}

void GraphvizModel::componentComplete()
{
    m_isComponentComplete = true;
    reloadGraph();
}

QVariant GraphvizModel::data(int row, int role) const
{
    if (row >= agnnodes(m_graph))
        return QVariant();

    if (role == DisplayRole || role == ShapeRole || role == PositionRole || role == SizeRole)
    {
        Agnode_t *node = agfstnode(m_graph);
        for (int i = 0; i < row; ++i)
            node = agnxtnode(m_graph, node);
        if (role == DisplayRole)
        {
            QString display(node->name);
            display += "(";
            Agedge_t *edge = agfstout(m_graph, node);
            while (edge)
            {
                display += edge->head->name;
                display += ", ";
                edge = agnxtout(m_graph, edge);
            }
            display += ")";
            return display;
        }
        if (role == ShapeRole)
                return QString(agget(node, (char *) "shape"));
        if (role == PositionRole)
        {
            QString str(agget(node, (char *) "pos"));
            QPoint point;
            if (!str.isEmpty())
            {
                QStringList pos = str.split(',');
                point.setX(pos.at(0).toInt());
                point.setY(pos.at(1).toInt());
            }
            return point;
        }
        if (role == SizeRole)
        {
            QString width(agget(node, (char *) "width"));
            QString height(agget(node, (char *) "height"));
            QRect rect;
            if (!width.isEmpty() && !height.isEmpty())
            {
                rect.setWidth(width.toFloat()*100);
                rect.setHeight(height.toFloat()*100);
            }
            return rect;
        }
    }
    return QVariant();
}

#define GRAPHVIZMODEL_MAX_REDIRECT 16

void GraphvizModel::requestFinished()
{
    emit progressChanged(1.0);

    m_redirectCount++;
    if (m_redirectCount < GRAPHVIZMODEL_MAX_REDIRECT)
    {
        QVariant redirect = m_reply->attribute(QNetworkRequest::RedirectionTargetAttribute);
        if (redirect.isValid())
        {
            QUrl url = m_reply->url().resolved(redirect.toUrl());
            m_reply->deleteLater();
            m_reply = 0;
            setSource(url);
            return;
        }
    }
    m_redirectCount = 0;

    if (m_reply->error() != QNetworkReply::NoError)
    {
        changeStatus(ErrorStatus, m_reply->errorString());

        disconnect(m_reply, 0, this, 0);
        m_reply->deleteLater();
        m_reply = 0;
    }
    else
    {
        QByteArray data = m_reply->readAll();
        disconnect(m_reply, 0, this, 0);
        m_reply->deleteLater();
        m_reply = 0;

        processDotGraph(data);
    }
}

void GraphvizModel::requestProgress(qint64 received, qint64 total)
{
    if (m_status == DownloadingStatus && total > 0)
        emit progressChanged(qreal(received)/total);
}

void GraphvizModel::reloadGraph()
{
    if (!m_isComponentComplete)
        return;

    if (!m_dot.isEmpty())
    {
        if (!m_source.isEmpty())
            qmlInfo(this) << "You have defined both source and dot properties. If both source and dot are set, dot is used.";
        processDotGraph(m_dot);
    }
    else if (!m_source.isEmpty())
    {
        requestSource();
    }
    else
    {
        processDotGraph("");
    }
}

void GraphvizModel::requestSource()
{
    changeStatus(DownloadingStatus);

    emit progressChanged(0.0);

    QNetworkRequest request(m_source);
    m_reply = qmlContext(this)->engine()->networkAccessManager()->get(request);
    QObject::connect(m_reply, SIGNAL(finished()), SLOT(requestFinished()));
    QObject::connect(m_reply, SIGNAL(downloadProgress(qint64,qint64)), SLOT(requestProgress(qint64,qint64)));
}

void GraphvizModel::layoutGraph()
{
    if (m_isComponentComplete && m_graphvizLayout != UnspecifiedLayout)
    {
        const char *layout[] = {"dot","neato","fdp","twopi","circo"};
        gvLayout(m_gvc, m_graph, layout[m_graphvizLayout-1]);
        gvRender(m_gvc, m_graph, layout[m_graphvizLayout-1], NULL);
        gvFreeLayout(m_gvc, m_graph);
    }
}

void GraphvizModel::processDotGraph(const QString &dotGraph)
{
    if (!dotGraph.isEmpty())
    {
        changeStatus(ProcessingStatus);

        QTemporaryFile tmpFile;
        if (!tmpFile.open())
        {
            changeStatus(ErrorStatus, tr("Error when creating temporary file !"));
            return;
        }
        tmpFile.write(dotGraph.toLocal8Bit());
        tmpFile.close();

        FILE *fp = fopen(tmpFile.fileName().toLocal8Bit().data(), "r");
        if (fp)
        {
            m_graph = agread(fp);
            fclose(fp);
            if (!m_graph)
            {
                changeStatus(ErrorStatus, tr("Error when parsing dot graph !"));
                return;
            }

            if (!m_source.isEmpty() && m_graphvizType != UnspecifiedType)
                qmlInfo(this) << "You have defined a type for you graph but it is been overriden by graph type acquired from dot file !";

            m_graphvizType = (AG_IS_DIRECTED(m_graph)) ?
                                    (AG_IS_STRICT(m_graph)) ?
                                            DigraphStrictType:DigraphType
                                    :
                                    (AG_IS_STRICT(m_graph)) ?
                                            GraphStrictType:GraphType;

            layoutGraph();
            emit countChanged();
        }
        else
        {
            changeStatus(ErrorStatus, tr("Unable to open dot temporary file %1").arg(tmpFile.fileName()));
            return;
        }
    }
    changeStatus(ReadyStatus);
}

void GraphvizModel::changeStatus(GraphvizModel::Status status, const QString &errorString)
{
    m_status = status;
    m_errorString = errorString;
    emit statusChanged(m_status);
}
