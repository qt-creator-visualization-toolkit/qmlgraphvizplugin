QMLDUMP=$$(QMLDUMP)
!isEmpty(QMLDUMP) {
    QMAKE_POST_LINK += $$(QMLDUMP) -path ./ 1.1 > plugins.qmltypes
    qmltypes.files = plugins.qmltypes
    qmltypes.path = $$installPath
    INSTALLS += qmltypes
} else {
    message("Please adjust QMLDUMP environment variable to qmldump absolute path")
}
