import QtQuick 1.1

Rectangle {
    width: 20
    height: 20
    border.color: "red"
    smooth: true
    property alias text: label.text
    Text {
        id: label
        smooth: true
        anchors.centerIn: parent
    }
    Behavior on x {
             NumberAnimation { duration: 1000 }
    }
    Behavior on y {
             NumberAnimation { duration: 500 }
    }
}
