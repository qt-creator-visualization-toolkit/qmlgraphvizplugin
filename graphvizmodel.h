#ifndef GRAPHVIZMODEL_H
#define GRAPHVIZMODEL_H

#include <QtCore/QObject>

#include <QtDeclarative/QtDeclarative>
#include <QtDeclarative/QDeclarativeParserStatus>

#include <graphviz/gvc.h>
#include <graphviz/graph.h>

class GraphvizModel : public QObject, public QDeclarativeParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QDeclarativeParserStatus)

    Q_ENUMS(GraphElementRole)
    Q_ENUMS(GraphvizType)
    Q_ENUMS(GraphvizLayout)
    Q_ENUMS(Status)
    Q_PROPERTY(GraphvizType type READ type)
    Q_PROPERTY(GraphvizLayout layout READ layout WRITE setLayout NOTIFY graphLayoutChanged)
    Q_PROPERTY(Status status READ status NOTIFY statusChanged)
    Q_PROPERTY(QUrl source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(QString dot READ dot WRITE setDot NOTIFY dotChanged)
    Q_PROPERTY(int count READ count NOTIFY countChanged)
    Q_PROPERTY(QString errorString READ errorString)
    Q_PROPERTY(QRect boundingRect READ boundingRect)
    Q_DISABLE_COPY(GraphvizModel)

public:
    GraphvizModel(QObject *parent = 0);
    ~GraphvizModel();

    // Enums
    enum GraphElementRole {DisplayRole = Qt::DisplayRole, ShapeRole = Qt::UserRole, PositionRole = Qt::UserRole+1, SizeRole = Qt::UserRole+2};
    enum GraphvizType {UnspecifiedType = 0, GraphType, GraphStrictType, DigraphType, DigraphStrictType};
    enum GraphvizLayout {UnspecifiedLayout = 0, DotLayout, NeatoLayout, FdpLayout, TwopiLayout, CircoLayout};
    enum Status {UnspecifiedStatus, ReadyStatus, DownloadingStatus, ProcessingStatus, ErrorStatus};

    // QML element methods
    GraphvizType type() const;
    GraphvizLayout layout() const;
    void setLayout(const GraphvizLayout &graphLayout);
    Status status() const;
    QUrl source() const;
    void setSource(const QUrl &source);
    QString dot() const;
    void setDot(const QString &dot);
    int count() const;
    QString errorString() const;
    QRect boundingRect() const;

    // Invokable methods
    Q_INVOKABLE QVariant data(int row, int role) const;

    // QMLDeclarativeParserStatus methods
    void classBegin();
    void componentComplete();

Q_SIGNALS:
    void graphLayoutChanged(GraphvizModel::GraphvizLayout);
    void statusChanged(GraphvizModel::Status);
    void sourceChanged();
    void dotChanged();
    void countChanged();
    void progressChanged(qreal progress);

private Q_SLOTS:
    void requestFinished();
    void requestProgress(qint64 received, qint64 total);

private:
    void reloadGraph();
    void requestSource();
    void layoutGraph();
    void processDotGraph(const QString &dotGraph);
    void changeStatus(Status status, const QString &errorString = "");

    // QML element attributes
    GraphvizModel::GraphvizType m_graphvizType;
    GraphvizModel::GraphvizLayout m_graphvizLayout;
    Status m_status;
    QUrl m_source;
    QString m_dot;

    QString m_errorString;

    // Graphviz-related attributes
    GVC_t *m_gvc;
    Agraph_t *m_graph;

    // misc
    bool m_isComponentComplete;
    QNetworkReply *m_reply;
    int m_redirectCount;
};

QML_DECLARE_TYPE(GraphvizModel)

#endif // GRAPHVIZMODEL_H
