#ifndef QMLGRAPHVIZPLUGIN_PLUGIN_H
#define QMLGRAPHVIZPLUGIN_PLUGIN_H

#include <QtDeclarative/QDeclarativeExtensionPlugin>

class QmlgraphvizpluginPlugin : public QDeclarativeExtensionPlugin
{
    Q_OBJECT
    
public:
    void registerTypes(const char *uri);
};

#endif // QMLGRAPHVIZPLUGIN_PLUGIN_H

