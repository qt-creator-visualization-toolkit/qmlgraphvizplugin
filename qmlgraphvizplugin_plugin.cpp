#include "qmlgraphvizplugin_plugin.h"
#include "graphvizmodel.h"

#include <QtDeclarative/QtDeclarative>

void QmlgraphvizpluginPlugin::registerTypes(const char *uri)
{
    // @uri org.liveblue.qmlcomponents.graphviz
    qmlRegisterType<GraphvizModel>(uri, 1, 1, "GraphvizModel");
}

Q_EXPORT_PLUGIN2(Qmlgraphvizplugin, QmlgraphvizpluginPlugin)

